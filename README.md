# QRshop
QRshop is a hostable repository for 3DS CIA files written in PHP.
Simply place your CIA files in the folder named "cia" and QRshop will list them on the index page.
When you find a CIA file in the list that you want to install just click on it to be shown a QR code for that file, which can be installed straight to the 3DS by scanning the code in FBI Installer.

## Requirements
- PHP 7

## Third Party Libraries
- [PHP QR Code](http://phpqrcode.sourceforge.net/) to generate QR codes locally.