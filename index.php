<?php

$config = require 'config.php';

if (isset($_GET['cwd'])) {
    $cwd = $_GET['cwd'];
} else {
    $cwd = 'cia';
};

$files = scandir($cwd);
$files = array_diff(scandir($cwd), array('.', '..'));

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QRShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col">

            <h1 class="font-weight-bold">QRShop!</h1>
            <p>Directory listing of /cia:</p>

            <ul class="list-unstyled">

                <?php foreach ($files as $file) :?>

                    <?php
                        $fileurl = urlencode('http://'.$config['HttpHost'].':'.$config['HttpPort'].'/cia/'.$file);
                    ?>

                    <li class="list-item">
                        <a href="qr.php?url=<?= $fileurl ?>"><?= $file ?></a>
                    </li>

                <?php endforeach; ?>

            </ul>
            
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col">Created by <a href="https://www.twitter.com/Alkalime_">Alkalime</a> #wow #whoa</div>
        </div>
    </div>
</footer>

</body>
</html>
